# USB-Purifier

> One of the major vectors for computer attacks are USB devices. These devices can carry viruses or physical components that can lead to data theft or even cause damage to a PC. To protect against these types of attacks while still being able to use USB devices, a tool is needed. The goal of this purifier is to have a portable tool that allows for safe and secure access to removable storage devices, such as keys and external hard drives. To achieve this, the tool will be designed to scan and notify any potential threats before granting access to the device.

## About
The USB-Purifier is a tool that enables safe access to the data stored on removable media devices. It scans for potential threats and alerts the user to any potential risks. In order to run this tool, a computer, such as a Raspberry Pi, is required.

![Render of usb-purifier](doc/img/render.png)

## Requirements

- Raspberry PI (or other computer)
- ClamAv
- PHP 7.4
- Redis
- Python3

## Setup

See INSTALL.md for a Raspberry Pi installation

## Sources

### src/purifier
folder containing the files used to start the purification process of a partition found on the OS.

The purifier-py.service file on src/system is to be placed under /etc/systemd/system to manage a service via the command :

``systemctl enable purifier-py.service``

### src/web_server
folder containing the JS and php scripts used by apache2.4 to provide resources to the client

### src/system
folder containing service file, running purifier as a daemon

## License, Credit
This project use tinyfilemanager as web server and php-redis-client as php-redis interface 
### [Tinyfilemanager](https://github.com/prasathmani/tinyfilemanager)
### [php-redis-client](https://github.com/cheprasov/php-redis-client)