# USB-Purifier
This document explains how to set up the project for a Raspberry Pi (4)

## Requirement
You need a raspberry pi with an OS already configured. This tutorial uses raspberry pi OS (debian 11.5)

## First step
It is necessary to download all the software and framework required to run the project, with :

    sudo apt install git python3 python3-pip clamav clamav-daemon apache2 libapache2-mod-php7.4 redis php-redis

    sudo pip3 install pyudev sh pyclamd redis

and download the current project with a ***git clone***

## Setup web server

First you need to install the php-redis-client module. To do this, create a directory :

    mkdir php-redis
    cd php-redis

Download the composer:

    wget -nc http://getcomposer.org/composer.phar

and setup dependency:

    php composer.phar require cheprasov/php-redis-client

now a vendor folder has been created, copy it to /var/www
  
    cp -R vendor /var/www

The contents of the src/web_server folder on usb-purifier project must be copied to /var/www/html to allow resources to be served by the apache2 server.

    cp -R src/web_server/* /var/www/html

go to /etc/apache2/apache2.conf and modify the config file to serve the /var/www/ and /media/ resources

    <Directory /var/www/>
        AllowOverride None
        Require all granted
    </Directory>

    <Directory /media/>
        Options Indexes
        AllowOverride None
        Require all granted
    </Directory>

configure the base file 000-default.conf under /etc/apache2/site-available to create a virtual host

    <VirtualHost *:80>
        ServerName usb-purifier.local
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html/
        Alias "/data" "/media"

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>


you can modify the dir.conf file under /etc/apache2/mods-available/ to access the purifier directly

    <IfModule mod_dir.c>
        DirectoryIndex webpage.php
    </IfModule>

restarts apache with :

    sudo systemctl restart apache2

now create a folder under /media to hold the system logs with :

    mkdir /media/.log 

## Setup analyser

Edit file src/system/purifier-py.service for your configuration and copy file to /etc/systemd/system to set up a service

    cp src/system/purifier-py.service /etc/systemd/system/
    sudo systemctl enable purifier-py.service
    sudo systemctl start purifier-py.service

## Testing access and use it
Try to access the purifier via your IP address or your *.local hostname e.g. usb-purififer.local/ and take advantage of the project
