# This class is used to manage the Redis database
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2022, School of Engineering and Architecture of Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Unity Tests of class:
- RedisManager
"""

import sys
sys.path.append('../../src/purifier')

import unittest
import random
from device_manager import DeviceManager

import unittest
from unittest.mock import MagicMock, patch
from pyudev import Context

class TestDeviceManager(unittest.TestCase):
    """Unit tests for DeviceManager class"""
    
    def setUp(self):
        """Initialize test resources"""
        self.cb_start = MagicMock()
        self.cb_stop = MagicMock()
        self.device_manager = DeviceManager(cb_start=self.cb_start, cb_stop=self.cb_stop)
        self.dev = MagicMock()
        self.dev.properties = {'DEVTYPE': 'test_product', 'ACTION': 'test_driver', 'ID_MODEL_FROM_DATABASE': 'test_model'}

    def tearDown(self):
        """Clean up test resources"""
        self.device_manager = None
        self.dev = None
        self.cb_start = None
        self.cb_stop = None

    def test_

if __name__ == '__main__':
    unittest.main()