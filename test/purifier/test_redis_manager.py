# This class is used to manage the Redis database
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2022, School of Engineering and Architecture of Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Unity Tests of class:
- RedisManager
"""

import sys
sys.path.append('../../src/purifier')

import unittest
import random
from redis_manager import RedisManager

# Fuzzing Function
# use case of https://www.fuzzingbook.org/html/Fuzzer.html
def fuzzer(max_length: int = 100, char_start: int = 32, char_range: int = 32) -> str:
    """A string of up to `max_length` characters
       in the range [`char_start`, `char_start` + `char_range`)"""
    string_length = random.randrange(0, max_length + 1)
    out = ""
    for i in range(0, string_length):
        out += chr(random.randrange(char_start, char_start + char_range))
    return out


class TestRedisManager(unittest.TestCase):

    def setUp(self):
        self.redis = RedisManager()

    def test_set_status(self):
        """test save status value on redis DB"""
        self.redis.set_status('status', 'data')
        self.assertEqual(self.redis.get_status('status'), b'data')

        for i in range(10):
            key = fuzzer()
            val = fuzzer()
            self.redis.set_status(key, val)
            self.assertEqual(self.redis.get_status(key), bytes(val, 'utf-8'))

    def test_set_infos(self):
        """test save infos value on redis DB"""
        self.redis.set_info('infos', 'data')
        self.assertEqual(self.redis.get_info('infos'), b'data')

        for i in range(10):
            key = fuzzer()
            val = fuzzer()
            self.redis.set_info(key, val)
            self.assertEqual(self.redis.get_info(key), bytes(val, 'utf-8'))

    def test_pubsub(self):
        """test publish and subscribe data value"""
        sub = self.redis.sub()
        sub.psubscribe("events")
        sub.get_message()

        value = b'{"event": "status", "name": "status", "data": "data"}'
        self.redis.pub_status('status', 'data')
        self.assertEqual(sub.get_message()['data'], value)

    def test_clear_db(self):
        """test clear Database"""
        self.redis.clear_db()
        self.assertEqual(self.redis.get_status('status'), None)
        self.assertEqual(self.redis.get_status('infos'), None)

if __name__ == '__main__':
    unittest.main()