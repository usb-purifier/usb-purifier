/*
*   server_event.js
*   @autor: William Margueron
*   @version: 0.1
*   
*   ServerEvent JS for purifer, this script implement (Server Send Event) browser side
*/

let serverEvent;

if (typeof(EventSource) == "undefined") {
    Alert("Error: Server-Sent Events are not supported in your browser");
}

// Check if already connected
if (typeof(serverEvent) !== "undefined") {
    serverEvent.close();
}

// Create Server-Sent event with events.php
serverEvent = new EventSource("events.php");

serverEvent.onopen = function() {
    console.log("SSE open");
}

serverEvent.onerror = function(event) {
    console.log("SSE error " + event.data);
}

serverEvent.addEventListener("status", (event) => {

    let jsonObj = JSON.parse(event.data);
    if(jsonObj == null){
        return;
    }
    
    // find element on doc and change value
    let elem = document.getElementById(jsonObj.name);
    if(elem != null){
        elem.innerHTML = jsonObj.data + get_status_icon(jsonObj.data);
    }
});

serverEvent.addEventListener("infos", (event) => {

    let jsonObj = JSON.parse(event.data);
    if(jsonObj == null){
        return;
    }

    // find element on doc and change value
    let elem = document.getElementById(jsonObj.name + "_info");
    if(elem != null){
        elem.innerHTML = jsonObj.data;
    }
});

// Reload Page
serverEvent.addEventListener("refresh", (event) => {
    serverEvent.close()
    window.location.reload();
});

// get current icon for status
function get_status_icon(status){
    if(status == "Clean"){
        return '<img class="element_status" src="img/clean.png">';
    } else if (status == "Dangerous"){
        return '<img class="element_status" src="img/virus.png">';
    } else if (status == "Analysis"){
        return '<img class="element_status" src="img/waiting.png">';
    }
    return "";
}