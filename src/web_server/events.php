<?php
/*
*   events.php
*   @autor: William Margueron
*   @version: 1.0
*   
*   Event manager for purifer, this script uses (Server Send Event)
*   When the browser requests this script, it subscribes to a redis broker
*   and sends the received data to the client web browser. 
*
*   Use library RedisClient: https://github.com/cheprasov/php-redis-client
*/

require (dirname(__DIR__).'/vendor/autoload.php');
use RedisClient\RedisClient;
 
// Setup header for event-stream (HTML5)
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

// Topic to subscribe
define("CHANNEL", "events");

// Create a redis client
$redis = new RedisClient([
    'timeout' => 20 // waiting answer for 20 seconds
]);

// Start connexion to redis database
if(!isset($redis)){
    alert("Error: connexion to event manager failed");
}

// Init connexion with a void data
send_event("", "data: void\n\n");

// Subscribe to events channel 
$redis->subscribe(CHANNEL, function($type, $channel, $msg) {

    if (!isset($type)) {
        // all 20s ping to keep connexion alive
        send_event("", "data: ping\n\n");
        return true;
    }

    // Decode Json msg
    $json = json_decode($msg);

    // check if json is valid
    if($json == null){
        return true;
    }

    // get event type, name and data
    $event = $json->{'event'};
    $name = $json->{'name'};
    $data = $json->{'data'};

    // create a JSON struct
    $send_msg = '{"name":"' . $name . '", "data":"' . $data .'"}';

    send_event($event, $send_msg);

    // Check if connexion down and close
    if(connection_aborted()){
        // return false to unsubscribe
        return false;
    }

    // return true to get next msg
    return true;
});

// Send data to client browser
function send_event($event, $data){
    echo("event: {$event}\n");
    echo("data: {$data}\n\n");  

    // Send data and flush buffer
    ob_end_flush();
    flush();
}

?>