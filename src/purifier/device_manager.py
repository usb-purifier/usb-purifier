#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2022, School of Engineering and Architecture of Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Management of devices connected to the purifier, in the case 
of a storage device mount the partitions present under the /media/ folder
"""

__authors__ = "William Margueron"
__version__= "1.0"

import logging
import json

from pyudev import Device
from redis_manager import RedisManager
from os import makedirs, rmdir, path
from sh import mount, umount


MOUNT_REPOSITORY = "/media"
NO_LABEL = "/USB"
SECTOR_SIZE = 512
GB_SIZE = 1e09

class DeviceManager:
    """A class that is used to manage device and partitions."""

    def __init__(self, cb_start = None, cb_stop = None):
        """ cb_start and cb_stop are parameters for callback functions with 
            a string (path) parameter for start and stop Analyser
        """
        self.__cb_start = cb_start
        self.__cb_stop = cb_stop
        self.__red = RedisManager("localhost")
        self.__mount_p = {}

        # Clear and set default status for purifier
        self.__red.clear_db()
        self.__red.pub_status("analyser", "Waiting")

    def __mount_partition(self, p_name: str, p_label: str):
        """mount partiton and call start callback function"""
        if (p_name in self.__mount_p):
            logging.error("error already mount " + p_name)
            return

        if p_label:
            mnt_p = MOUNT_REPOSITORY + "/" + p_label
        else:
            mnt_p = MOUNT_REPOSITORY + NO_LABEL + str(len(self.__mount_p))
        # if path use add "1"
        if (mnt_p in self.__mount_p.values()):
            mnt_p += "1"
        self.__mount_p[p_name] = mnt_p

        try:
            makedirs(mnt_p, exist_ok=True)
            if(path.ismount(mnt_p)):
                umount(mnt_p)
            mount(p_name, mnt_p, "-o", "umask=000")

            # if callback is valid
            if self.__cb_start:
                self.__cb_start(mnt_p)
        except:
            logging.error("error mount " + p_name)

    def __umount_partition(self, p_name: str):
        """call stop callback function and umount partition"""
        if p_name in self.__mount_p:
            if self.__cb_stop:
                self.__red.pub_status("analyser", "Stopping " + self.__mount_p[p_name] + " analysis")
                self.__cb_stop(self.__mount_p[p_name])
            
            try:
                umount(self.__mount_p[p_name])
                rmdir(self.__mount_p[p_name])
            except:
                logging.error("error unmount " + p_name)

            self.__mount_p.pop(p_name)
        else:
            logging.error("Remove not mounted device")

    def add_device(self, dev: Device):
        """add device to database and notify webserver"""
        dev_id = dev.properties.get('PRODUCT')
        if dev_id is None:
            logging.error("Device has no product ID")
            return
  
        # Get Driver and Model
        dev_type = dev.properties.get('DRIVER')
        dev_model = dev.properties.get('ID_MODEL_FROM_DATABASE')
        
        # Create Data Structure
        data = {"id": dev_id, "type": dev_type, "model": dev_model}
        
        save_devs = self.__red.get_status("devices")
        devices = []

        if(save_devs is not None):
            # load current devices
            devices = json.loads(save_devs)
            if(devices is not None):
                for item in devices:
                    # if alredy save return
                    if(("id" in item) and (item["id"] == dev_id)):
                        return
            else:
                devices = []
        # add data and notify webserver
        devices.append(data)
        self.__red.set_status("devices", json.dumps(devices))
        self.__red.pub_status("analyser", "Device added")
        logging.info("[Device: " + dev_id + " added]")

    def rm_device(self, dev: Device):
        """remove device to database and notify webserver"""
        dev_id = dev.properties.get('PRODUCT')
        if dev_id is None:
            logging.error("Device has no product ID")
            return

        save_devs = self.__red.get_status("devices")

        if(save_devs is None):
            return
        
        devices = json.loads(save_devs)
        if(devices is not None):
            for item in devices:
                if(("id" in item) and (item["id"] == dev_id)):
                    devices.remove(item)
                    self.__red.set_status("devices", json.dumps(devices))
                    self.__red.pub_status("analyser", "Device removed")
                    logging.info("[Device: " + dev_id + " removed]")        

    def add_partition(self, part: Device):
        """add partition to database and notify webserver"""
        p_name = part.properties.get('DEVNAME')
        if p_name is None:
            logging.error("Partition has no devname")
            return
        
        # Get infos
        p_label = part.properties.get('ID_FS_LABEL')
        p_type = part.properties.get('ID_FS_TYPE')
        p_size = part.properties.get('ID_PART_ENTRY_SIZE')

        if p_size is not None:
            # convert size (GB)
            p_size = str(round(float(int(p_size) * SECTOR_SIZE) / GB_SIZE, 2)) + " GB"           

        # Create Data Structure
        data = {"name": p_name, "label": p_label, "type": p_type, "size": p_size}

        save_parts = self.__red.get_status("partitions")
        partitions = []

        if(save_parts is not None):
            partitions = json.loads(save_parts)
            if(partitions is not None):
                for item in partitions:
                    if(("name" in item) and (item["name"] == p_name)):
                        return
            else:
                partitions = []
        
        partitions.append(data)
        self.__mount_partition(p_name, p_label)
        self.__red.set_status("partitions", json.dumps(partitions))

        self.__red.pub_status("analyser", "Partition added")
        logging.info("[Partitions: " + p_name + " added]")  

    def rm_partition(self, part: Device):
        """remove partition to database and notify webserver"""
        p_name = part.properties.get('DEVNAME')
        if p_name is None:
            logging.error("Partition has no devname")
            return
        
        save_parts = self.__red.get_status("partitions")

        if(save_parts is None):
            return

        partitions = json.loads(save_parts)
        if(partitions is not None):
            for item in partitions:
                if (("name" in item) and (item["name"] == p_name)):
                    partitions.remove(item)
                    self.__umount_partition(p_name)
                    self.__red.set_status("partitions", json.dumps(partitions))
                    self.__red.pub_status("analyser", "Partition removed")
                    logging.info("[Partitions: " + p_name + " removed]")

    def device_log(self, dev: Device):
        """Log current device properties"""
        name = dev.properties.get('DRIVER')
        logging.info("<" + str(name) + ">")
        for item in iter(dev.properties):
            logging.info("  " + dev.get(item))
    
    def __call__(self, dev: Device):
        """Check device and call proper function"""
        if((dev is None) or (dev.properties is None)):
            logging.error("Device or properties object is null")
            return

        # HID or Storage devices
        if((dev.properties.get('DEVTYPE')) == "usb_interface"):
            if((dev.properties.get('ACTION')) == "add"):
                self.add_device(dev)
                self.__red.pub_refresh_page()

            if((dev.properties.get('ACTION')) == "remove"):
                self.rm_device(dev)
                # all disk is remove => change to waiting status
                if(len(self.__mount_p) < 1):
                    self.__red.clear_db()
                    self.__red.pub_status("analyser", "Waiting")
                    
                self.__red.pub_refresh_page()

        # Partitions detected
        if(dev.properties.get('DEVTYPE') == "partition"):
            if((dev.properties.get('ACTION')) == "add"):
                self.add_partition(dev)
                self.__red.pub_refresh_page()

            if((dev.properties.get('ACTION')) == "remove"):
                self.rm_partition(dev)
            
        self.device_log(dev)