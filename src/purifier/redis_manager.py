# This class is used to manage the Redis database
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2022, School of Engineering and Architecture of Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Management of the connection to the redis database with a class 
allowing the publication and retrieval of data
"""

__authors__ = "William Margueron"
__version__= "1.0"

from redis import StrictRedis
import json

class RedisManager:
    """A class that is used to manage the Redis database."""

    def __init__(self, host = 'localhost', port = 6379):
        self.__host = host
        self.__port = port
        self.__r = StrictRedis(host=self.__host, port=self.__port, db=0)

    def __del__(self):
        self.__r.close()

    def __set_element(self, e_type: str, e_name: str, e_data: str):
        self.__r.hset(e_type, e_name, e_data)

    def __pub_element(self, e_type: str, e_name: str, e_data: str):
        event = {"event": e_type, "name": e_name, "data": e_data}
        self.__r.publish('events', json.dumps(event))

    def __get_element(self, e_type: str, e_name: str):
        return self.__r.hget(e_type, e_name)

    def set_status(self, name: str, data: str):
        """save the status on the database"""
        self.__set_element("elements_status", name, data)

    def pub_status(self, name: str, data: str):
        """publish a status on the broker and save it"""
        self.__pub_element("status", name, data)
        self.set_status(name, data)

    def set_info(self, name: str, data: str):
        """save information on the database"""
        self.__set_element("elements_info", name, data)
    
    def pub_info(self, name: str, data: str):
        """publish information on the broker and save it"""
        self.__pub_element("infos", name, data)
        self.set_info(name, data)

    def get_status(self, name: str):
        """return a string containing the status of the element"""
        return self.__get_element("elements_status", name)
    
    def get_info(self, name: str):
        """return a string containing information of the element"""
        return self.__get_element("elements_info", name)

    def pub_refresh_page(self):
        """publish a request to reload the web page"""
        self.__pub_element("refresh", "", "")

    def clear_db(self):
        """flush the database"""
        self.__r.flushall()

    def sub(self):
        """return pubsub module"""
        return self.__r.pubsub()
