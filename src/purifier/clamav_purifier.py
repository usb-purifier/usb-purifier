#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2022, School of Engineering and Architecture of Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
System of purification of a file with the clamAV tool integrated in python
"""

import os
import json
import logging
import time

from pyclamd import ClamdUnixSocket
from pathlib import Path
from typing import Iterator

from redis_manager import RedisManager
from multiprocessing import Process, Event

__authors__ = "William Margueron"
__version__= "0.1"


class PurifierBase(Process):
    """
    PurifierBase, this class implemente a base process to analyse files
    """
    def __init__(self):
        super(PurifierBase, self).__init__()
        self._red = RedisManager()
        self._clamAv = ClamdUnixSocket()
        self._exit = Event()

    def terminate(self):
        """Exit cleany"""
        self._exit.set()

    def process_file(self, src_file: str) -> bool:
        """Process an individual file."""

        self._red.pub_status(src_file, "Analysis")
        logging.info("Processing : " + src_file)
        
        # Ask status of file to clamAv
        result = self._clamAv.scan_file(src_file)
        
        if result is None:
            self._red.pub_status(src_file, "Clean")
            self._red.pub_info(src_file, "")
            return False
        else:
            self._red.pub_status(src_file, "Dangerous")
            self._red.pub_info(src_file, str(result[src_file]))
            return True

    def run(self):
        pass

class PurifierProcess(PurifierBase):
    """
    A subclass of PurifierBase, this class starts an analysis of the files present in a folder 
    and communicates to the redis database the state of the files with details of 
    the different threats found.
    """
    def __init__(self, src_path: str):
        super(PurifierProcess, self).__init__()
        self.__root_path = src_path
        self._repo_analysis = {"/media"}
        self._files_dangerous = []

    def analysis_repo(self, path: str):
        """Change status of current tree directory"""
        split = path.split("/")
        src = ""
        for item in split:
            if not item:
                continue
            src += "/" + item
            if(src not in self._repo_analysis):
                self._repo_analysis.add(src)
                self._red.pub_status(src, "Analysis")

    def list_all_files(self, directory_path: Path) -> Iterator[Path]:
        """Walk on directory and return each of the file found"""
        for root, dirs, files in os.walk(directory_path, topdown = False):
            self.analysis_repo(str(root))
            
            for filename in files:
                yield Path(root) / filename

            # if dangerous file found change directory status
            if any(str(root) in repo for repo in self._files_dangerous):
                self._red.pub_status(str(root), "Dangerous")
            else:
                self._red.pub_status(str(root), "Clean")

    def process_dir(self, src_path: str):
        """Process a directory on the source path"""
        src_dir = Path(src_path)
        # publish current src analyse
        self._red.pub_status("analyser", "Analysis " + str(src_dir))

        # process each file
        for srcpath in self.list_all_files(src_dir):
            if self._exit.is_set():
                return
            
            if self.process_file(str(srcpath)):
                self._files_dangerous.append(str(srcpath))

        self._red.pub_status("analyser", "Done " + str(src_dir))

    def run(self):
        """Scan directory"""
        print("Start Process " + self.__root_path)
        self.process_dir(self.__root_path)
        print("End Process " + self.__root_path)

class PurifierEvent(PurifierBase):
    """
    A subclass of PurifierBase, this class listen request of anaysis
    """
    def __init__(self, callback = None):
        super(PurifierEvent, self).__init__()
        self._sub = self._red.sub()

    def process_event(self, data):
        """Process a received event"""
        event = json.loads(data)
        if event is None:
            return
        
        if ('event' in event) and ('name' in event):
            if (event['event'] == "analyse"):
                src_file = str(event['name'])
                self.process_file(src_file)
    
    def run(self):
        """
        subscribe to channel events 
        to get a file scan request
        """ 
        print("Start Process Event Purifier")
        self._sub.psubscribe("events")

        while True:
            if self._exit.is_set():
                break

            m = self._sub.get_message(timeout=1.0)
            if m is not None:
                if ('data' in m) and (isinstance(m['data'], (bytearray, bytes))):
                    self.process_event(m['data'])

        print("End Process Event Purifier")

class PurifierManager():
    """
    A class to manage multiprocess of partition analysis
    """
    def __init__(self):
        self.__event = None
        self.__process = {}

    def start_analyser(self, src_path: str):
        """
        start the analysis of a specific source file
        and Purifier event analyser if not started
        """
        print("Request Start: " + src_path)
        if self.__event is None:
            self.__event = PurifierEvent()
            self.__event.start()

        self.__process[src_path] = PurifierProcess(src_path)
        self.__process[src_path].start()

    def stop_analyser(self, src_path : str):
        """
        stop the analysis of a specific source file
        and Purifier event analyser
        """
        print("Request Stop: " + src_path)
        self.__process[src_path].terminate()
        self.__process[src_path].join()
        self.__process.pop(src_path)

        if (len(self.__process) == 0):
            if self.__event is not None:
                self.__event.terminate()
                self.__event.join()
                self.__event = None