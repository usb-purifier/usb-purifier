#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2022, School of Engineering and Architecture of Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
USB Purificator, Start a Device_manager and call files analyser
"""

__authors__ = "William Margueron"
__version__= "0.1"

import logging
import signal
import time
from logging.handlers import RotatingFileHandler
from device_manager import DeviceManager
from pyudev import Context, Monitor, MonitorObserver
from clamav_purifier import PurifierProcess, PurifierEvent, PurifierManager

LOGFILE = "/media/.log/purifier.log"

purifier = PurifierManager()
dev_manager = DeviceManager(purifier.start_analyser, purifier.stop_analyser)

context = Context()
monitor = Monitor.from_netlink(context)
observer = MonitorObserver(monitor, callback=dev_manager, name='monitor-observer')

def sigterm_terminate(_signo, _stack_frame):
    observer.send_stop()


if __name__ == "__main__":
    # Start logging system
    print("Start USB Purificator")
    # create a rotary file log with 10 backup
    logging.basicConfig(
        handlers=[RotatingFileHandler(LOGFILE, maxBytes=1e07, backupCount=10)],
        encoding='utf-8',
        level=logging.DEBUG,
        format='[%(asctime)s] %(levelname)s %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')

    signal.signal(signal.SIGTERM, sigterm_terminate)
    signal.signal(signal.SIGINT, sigterm_terminate)

    # Create a Observer with OS /dev event
    observer.start()
    observer.join()
    print("Stop USB Purificator")