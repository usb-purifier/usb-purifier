#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2021, School of Engineering and Architecture of Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Test multiprocessing detectUSB
"""

from pyudev import Context, Monitor
from multiprocessing import Manager, Value, Process
from ctypes import c_char_p
from flask import Flask, request, jsonify

# Flask Server
app = Flask(__name__)

def Process1(value):
    """Detection of USB Key"""
    context = Context()
    monitor = Monitor.from_netlink(context)
    monitor.filter_by('block')
    for device in iter(monitor.poll, None):
        if 'ID_FS_TYPE' in device:
            value.value = str(device.action) + " " + str(device.get('ID_FS_LABEL'))

# Flask response
@app.get("/status")
def get_status():
    return str(val.value)

if __name__ == '__main__':

    manager = Manager()
    # Shared memory (string)
    val = manager.Value(c_char_p, "void")
    process = Process(target=Process1, args=(val,))
    process.start()
    app.run(debug=True)
