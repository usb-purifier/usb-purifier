#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2021, School of Engineering and Architecture of Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Test REST API with Flask server
"""

#export FLASK_APP=test-app.py

from flask import Flask, request, jsonify
from random import randint

app = Flask(__name__)

status = {'status': 'waiting', 'current_action': 'wait'}

@app.get("/status")
def get_status():

    status["current_action"] = "wait " + str(randint(0, 10))
    return jsonify(status)

