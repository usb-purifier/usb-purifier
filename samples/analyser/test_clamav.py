import pyclamd

cd = pyclamd.ClamdAgnostic()
print(cd.ping())

print(cd.version())
print(cd.reload())
print(cd.stats().split()[0])
void = open('/tmp/EICAR','w').write(str(cd.EICAR()))
void = open('/tmp/NO_EICAR','w').write(str('no virus in this file'))

print(cd.scan_file('/tmp/EICAR'))
print(cd.scan_file('/tmp/NO_EICAR'))
print(cd.scan_stream(cd.EICAR()))