import pycirclean

# Create a PyCirclen client
client = pycirclean.Client()

# Scan a file for malware
scan_result = client.scan_file('/media/usb.log')

# Check if the file is malware
if scan_result.malicious:
    print('File is malware!')
else:
    print('File is safe.')