
import redis
import json
import sys

# construct python-redis interface
r = redis.StrictRedis(host='localhost', port=6379, db=0)

if(str(sys.argv[1]) == "clear"):
    r.flushall()
    r.close()
    exit()

data = {"event": str(sys.argv[1]), "name": str(sys.argv[2]), "data": "[{\"type\":\"ok\",\"model\":\"okok\"}]"}

# create and pass data to redis
r.publish('events', json.dumps(data))
r.hset("elements_status", str(sys.argv[2]), "[{\"type\":\"ok\",\"model\":\"okok\"}]")
r.close()