#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2021, School of Engineering and Architecture of Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Test multiprocessing detectUSB
"""

from pyudev import Context, Monitor
from multiprocessing import Process
from sh import mount, umount
import redis
import json
import time
import os

def send_event(redis, s_type, name, data):
    send_v = {"event": s_type, "name": name, "data": data}
    redis.publish('events', json.dumps(send_v))

def save_status(redis, name, data):
    redis.hset("elements_status", name, data)

def Process1():
    """Detection of USB Key"""
    context = Context()
    monitor = Monitor.from_netlink(context)
    monitor.filter_by('block')

    # construct python-redis interface
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    data = {"event": "status", "name": "devices", "data": ""}

    for device in iter(monitor.poll, None):
        info = " " + str(device.action) + " partition " + str(device.get('ID_FS_LABEL'))
        info_p = "name: " + str(device.get('ID_FS_LABEL')) + " type: " + str(device.get('ID_PART_TABLE_TYPE'))
        info_d = "driver: " + str(device.get('ID_USB_DRIVER')) + " usage: " + str(device.get('ID_FS_USAGE'))

        send_event(r, "status", "analyser", info)
        save_status(r, "analyser", info)
        send_event(r, "status", "devices", info_d)
        save_status(r, "devices", info_d)

        if(device.get('DEVTYPE') == "partition"):
            if(device.action == "add"):
                mount(device.get('DEVNAME'), "/media/USB")

            send_event(r, "refresh", "", "")

def Process2():
        # construct python-redis interface
    r = redis.StrictRedis(host='localhost', port=6379, db=0)

    while(True):
        time.sleep(15)
        send_event(r, "status", "", "")


if __name__ == '__main__':

    process = Process(target=Process1, args=())
    process2 = Process(target=Process2, args=())
    process.start()
    process2.start()
    process.join()
    process2.join()
