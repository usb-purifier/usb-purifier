import redis
import json
import sys

# construct python-redis interface
r = redis.StrictRedis(host='localhost', port=6379, db=0)
sub = r.pubsub()

sub.psubscribe("events")

for m in sub.listen():
    print(m)