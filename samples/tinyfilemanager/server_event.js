/*
*   server_event.js
*   @autor: William Margueron
*   @version: 0.1
*   
*   Event manager for purifer, this script implement (Server Send Event)
*/


var serverEvent;

if (typeof(EventSource) == "undefined") {
    Alert("Error: Server-Sent Events are not supported in your browser");
}

// Check if already connected
if (typeof(serverEvent) !== "undefined") {
    serverEvent.close();
}

// Create Server-Sent event with refresh.php
serverEvent = new EventSource("refresh.php");

serverEvent.onopen = function() {
    console.log("SSE open");
}

evtSource.onmessage = (event) => {
    console.log(event.data);
}

serverEvent.onerror = function(event) {
    console.log("SSE error " + event);
}

serverEvent.addEventListener("status", (event) => {

    let jsonObj = JSON.parse(event.data);
    if(jsonObj == null){
        return;
    }

    // find element on doc and change value
    let elem = document.getElementById(jsonObj.name);
    if(elem != null){
        elem.innerHTML = jsonObj.data;
    }
});

serverEvent.addEventListener("infos", (event) => {

    let jsonObj = JSON.parse(event.data);
    if(jsonObj == null){
        return;
    }

    // find element on doc and change value
    let elem = document.getElementById(jsonObj.name);
    if(elem != null){
        elem.innerHTML = jsonObj.data;
    }
});

serverEvent.addEventListener("refresh", (event) => {
    window.location.reload();
});