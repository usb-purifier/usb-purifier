<?php
/*
*   refresh.php
*   @autor: William Margueron
*   @version: 0.1
*   
*   Event manager for purifer, this script uses (Server Send Event)
*   When the browser requests this script, it subscribes to a redis broker
*   and sends the received data to the client web browser. 
*/
 
// Setup header for event-stream (HTML5)
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

define("CHANNEL", "events");

// Create a redis client
$redis = new Redis();

// Start connexion to redis database
if(!$redis->pconnect('127.0.0.1', 6379)){
    alert("Error: connexion to event manager failed");
}

// Subscribe to events channel 
$redis->subscribe([CHANNEL], function($link, $channel, $msg) {

    // Decode Json msg
    $json = json_decode($msg);

    if($json == null){
        return;
    }

    // get event type, name and data
    $event = $json->{'event'};
    $name = $json->{'name'};
    $data = $json->{'data'};

    // create a JSON struct
    $send_msg = '{"name":"' . $name . '", "data":"' . $data .'"}';

    echo("event: {$event}\n");
    echo("data: {$send_msg}\n\n");  

    // Send data and flush buffer
    ob_end_flush();
    flush();
});

?>